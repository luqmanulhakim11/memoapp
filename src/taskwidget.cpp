#include <taskwidget.h>
#include <ui_taskwidget.h>
#include <taskdata.h>

TaskWidget::TaskWidget(QWidget *parent) : QWidget(parent),
                                          ui(new Ui::TaskWidget)
{
  ui->setupUi(this);
  connect(ui->addTaskButton, &QPushButton::clicked, this, &TaskWidget::addTask);
  connect(ui->backButton, &QPushButton::clicked, [this] { this->close(); });
  TaskData::createFileJson("tasks");
}

TaskWidget::~TaskWidget()
{
  delete ui;
}

void TaskWidget::closeEvent(QCloseEvent *event)
{
  TaskData::writeTasks(mTasks);
  emit quitting();
}

void TaskWidget::addTask()
{
  bool ok;// bool for string
  // Input dialog
  QString name = QInputDialog::getText(this, tr("Add task"), tr("Task name"), QLineEdit::Normal, tr("Untitiled task"), &ok);

  if (ok && !name.isEmpty()) {
    qDebug() << "Adding new task";
    // Create  a task
    Task *task = new Task(name);
    // Listen for the remove signal
    connect(task, &Task::removed, this, &TaskWidget::removeTask);
    // Listen for task status change
    connect(task, &Task::statusChanged, this, &TaskWidget::taskStatusChanged);
    // append the task to vector
    mTasks.append(task);
    // Display the task
    ui->tasksLayout->addWidget(task);
    this->updateStatus();
  }
}

void TaskWidget::removeTask(Task *task)
{
  // Remove from vector
  mTasks.removeOne(task);
  // Remove from layout
  ui->tasksLayout->removeWidget(task);
  // Set ownership to null
  task->setParent(0);
  // delete pointer
  delete task;
  this->updateStatus();
}

void TaskWidget::taskStatusChanged(Task * /*task*/)
{
  this->updateStatus();
}

void TaskWidget::updateStatus()
{
  int completedCount = 0;
  for (auto t : mTasks) {
    if (t->isCompleted()) {
      completedCount++;
    }
  }
  // Count oustanding tasks
  int todoCount = mTasks.size() - completedCount;
  // Reset the text
  ui->statusLabel->setText(
    QString("Status: %1 todo / %2 completed").arg(todoCount).arg(completedCount));
}
