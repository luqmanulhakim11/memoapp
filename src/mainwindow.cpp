#include <mainwindow.h>// Class header
#include <ui_mainwindow.h>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);// Setups the ui by passing MainWindow (genius!)
  // Connect the addTask button to a task
  connect(ui->tasks, &QPushButton::clicked, this, &MainWindow::activateTasks);
  connect(ui->quitButton, &QPushButton::clicked, [this] { this->close(); });
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::activateTasks()
{
  TaskWidget *tasks = new TaskWidget();
  this->hide();
  tasks->showNormal();
  connect(tasks, &TaskWidget::quitting, [this] { this->show(); });
}
