#include <task.h>
#include <ui_task.h>
#include <taskdata.h>

Task::Task(const QString &name, QWidget *parent) : QWidget(parent),
                                                   ui(new Ui::Task)
{
  ui->setupUi(this);
  setName(name);
  // On click activate rename method
  connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
  auto niceName = [](const QString &taskName) -> QString { return "-----" + taskName.toUpper(); };
  // On click emit signall
  connect(ui->removeButton, &QPushButton::clicked, [this, name, niceName] {
        qDebug() << "Removing" << niceName(name);
        emit removed(this); });
  // On toggle call method
  connect(ui->checkBox, &QCheckBox::toggled, this, &Task::checked);
}

Task::~Task()
{
  delete ui;
}

void Task::rename()
{
  qDebug() << "Renaming!";
  bool ok;
  QString value = QInputDialog::getText(this, tr("Edit task"), tr("Task name"), QLineEdit::Normal, this->name(), &ok);
  if (ok && !value.isEmpty())
    setName(value);
}

void Task::setName(const QString &name)
{
  ui->checkBox->setText(name);
}

QString Task::name() const
{
  return ui->checkBox->text();
}

bool Task::isCompleted() const
{
  return ui->checkBox->isChecked();
}

void Task::onCreateTask()
{
}

void Task::checked(bool checked)
{
  QFont font(ui->checkBox->font());
  font.setStrikeOut(checked);
  ui->checkBox->setFont(font);
  emit statusChanged(this);
}
