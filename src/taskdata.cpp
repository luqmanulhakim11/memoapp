#include <QDebug>
#include <taskdata.h>

QString TaskData::mFileName;
QJsonDocument jsonDoc;

TaskData::TaskData()
{
}

// static
bool TaskData::createFileJson(QString filename)
{
  filename += ".json";
  QFile file(filename);
  if (file.open(QIODevice::ReadWrite | QIODevice::Text)) {
    qDebug() << filename << " is open!";
  }

  file.close();
  if (file.exists()) {
    mFileName = filename;
    return true;
  } else
    return false;
}

// Static
void TaskData::writeTasks(QVector<Task *> &tasks, QString vFileName)
{
  /* This function writes current task from QVector
   * -> JSON file created by createFileJson 
   * calls parseTasks function */

  QJsonDocument doc(parseTasks(tasks));
  QFile file(mFileName);
  QString temp = doc.toJson();
  // Remove file if exists to avoid a bug
  if (file.exists()) {
    file.remove();
  }
  if (file.open(QIODevice::ReadWrite | QIODevice::Text)) {
    QTextStream stream(&file);
    stream << temp;
  }
}

// Static
QJsonDocument TaskData::parseTasks(QVector<Task *> &tasks)
{
  /* This function parses tasks from QVector
   * -> returns a QJsonDocument */
  QVector<QJsonObject> temp;
  for (auto task : tasks) {
    QJsonObject mtemp;
    mtemp["name"] = task->name();
    mtemp["completed"] = "false";
    temp.append(mtemp);
  }

  QJsonObject mtemp;
  int i = 1;
  for (auto task : temp) {
    QString name = "task ";
    name += (signed int)i;
    mtemp[name] = task;
    i++;
  }

  return QJsonDocument(mtemp);
}
