#ifndef TASKWIDGET_H
#define TASKWIDGET_H

#include <QWidget>
#include <QInputDialog>
#include <QDebug>
#include <QVector>
#include <task.h>


namespace Ui {
class TaskWidget;
}

class TaskWidget : public QWidget
{
  Q_OBJECT

public:
  explicit TaskWidget(QWidget *parent = nullptr);
  ~TaskWidget();

  void closeEvent(QCloseEvent *event) override;
  void updateStatus();
public slots:
  void addTask();
  void removeTask(Task *task);
  void taskStatusChanged(Task *task);

signals:
  void quitting();

private:
  Ui::TaskWidget *ui;
  QVector<Task *> mTasks;
};

#endif// TASKWIDGET_H
