/* This class is a singleton */
#ifndef TASKDATA_H
#define TASKDATA_H

#include <QObject>
#include <QFile>
#include <QString>
#include <QVector>
#include <QJsonDocument>
#include <QJsonObject>
#include <task.h>

class TaskData : public QObject
{
  Q_OBJECT

public:
  // Static functions
  static bool createFileJson(QString filename);
  static void writeTasks(QVector<Task *> &tasks, QString vFileName = mFileName);


private:
  TaskData();
  TaskData(TaskData &);
  void operator=(TaskData &);
  static QJsonDocument parseTasks(QVector<Task *> &tasks);
  static QJsonDocument jsonDoc;
  static QString mFileName;
};

#endif
